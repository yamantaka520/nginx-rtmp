NGINX RTMP 套件
    Nginx: 1.18.0
    RTMP Module: 1.2.2

## 1. nginx.conf 設定範例
#### 設定範例
  預設設定
  ```
    worker_processes auto;
    access_log /var/log/nginx/rtmp_access.log;
    error_log  /var/log/rtmp_error.log debug;
    rtmp_auto_push on;
    events {
        worker_connections  1024;
    }
    rtmp {
        server {
            listen 1935;
            listen [::]:1935 ipv6only=on;    

            application live {
                live on;
                record off;
            }
        }
    }
  ``` 
  多平台推送設定
  ```
    worker_processes auto;
    access_log /var/log/nginx/rtmp_access.log;
    error_log  /var/log/rtmp_error.log debug;
    rtmp_auto_push on;
    events {
        worker_connections  1024;
    }
    rtmp {
        server {
            listen 1935;
            listen [::]:1935 ipv6only=on;
            chunk_size 4096;    

            application live {
                live on;
                record off;
            }
            application live2 {
                live on;
            
                meta copy;
                push rtmp://site1/streamkey;
                push rtmp://site2/streamkey;
                push rtmp://site3/streamkey;
            }
        }
    }
  ```

## 2. 檔案預設路徑
  ```

    error_log /var/log/rtmp_error.log
    nginx.conf /etc/nginx/nginx.conf
  ```